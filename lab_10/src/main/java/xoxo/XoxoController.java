package xoxo;

import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;
import xoxo.exceptions.*;
import xoxo.util.XoxoMessage;

import javax.swing.*;
import java.io.*;
import java.util.Scanner;

/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        gui.setEncryptFunction(e -> encryptMessage());
        gui.setDecryptFunction(e -> decryptMessage());
    }

    public void EncryptMessage() {
        String teks = gui.getMessageText();
        String kissKey = gui.getKeyText();
        String seed = gui.getSeedText();
        XoxoEncryption eMsg = null;
        try {
            eMsg = new XoxoEncryption(kissKey);
        } catch (KeyTooLongException e) {
            gui.setWarning("Key length must not exceed 28");
        } catch (InvalidCharacterException e) {
            gui.setWarning("Key can only contain A-Z, a-z, and @");
        }
        XoxoMessage pesan = null;
        try {
            if (seed.equals("") || Integer.parseInt(seed) == 18) {
                pesan = eMsg.encrypt(teks);
                seed = "18";
            } else {
                pesan = eMsg.encrypt(teks, Integer.parseInt(seed));
            }
        } catch (RangeExceededException e) {
            gui.setWarning("Seed must be a number between 0 and 36 (inclusive)");
        } catch (NumberFormatException e) {
            gui.setWarning("Seed must be a number!");
        } catch (SizeTooBigException e) {
            gui.setWarning("Message size too big!(Max:10 Kbit)");
        } catch (NullPointerException e) {
            gui.setWarning("Null pointer! Please check your program again.");
        }
        buatEncryptfile(message, seed);
    }
    public void DecryptMessage() {
        String hugKey = gui.getKeyText();
        String encrypted = gui.getMessageText();
        XoxoDecryption dMsg = new XoxoDecryption(hugKey);
        int seed;
        if (gui.getSeedText().equals("") || Integer.parseInt(gui.getSeedText()) == 18) {
            seed = DEFAULT_SEED;
        } else {
            seed = Integer.parseInt(gui.getSeedText());
        }
        String decrypted = dMsg.decrypt(encrypted, seed);
        buatDecryptfile(decrypted);
    }

    public void buatDecryptfile(String decryptMessage){
        File file1 = new File1(System.getProperty("user.dir") + "\\lab_10\\src\\main\\java\\Hasil\\fileHasilDecrypt.txt\\");
        try {
            file1.createNewFile();C:\Users\Jeremy\Desktop\DDP-2\Lab DDP2\lab_10\src\main\java\Hasil
            FileWriter tulis = new FileWriter(file1, true);
            tulis.write(dMsg + System.lineSeparator());
            tulis.flush();
            tulis.close();
            gui.appendLog("Decrypted text: " + dMsg);
        } catch (IOException e) {
            gui.appendLog("Decrypt failed! Cannot write file.");
        }

    }
    public void buatEncryptfile(XoxoMessage encryptMessage, String seed) {
        File file1 = new File(System.getProperty("user.dir") + "\\lab_10\\src\\main\\java\\Hasil\\fileHasilEncrypt.enc\\");
        try {
            file1.createNewFile();
            FileWriter tulis = new FileWriter(file1, true);
            tulis.write(eMsg.getEncryptedMessage() + " " + eMsg.getHugKey().getKeyString() + " " + seed + System.lineSeparator());
            tulis.flush();
            tulis.close();
            gui.appendLog("Encrypted text: " + eMsg.getEncryptedMessage());
            gui.appendLog("Hug key: " + eMsg.getHugKey().getKeyString());
            gui.appendLog("Seed: " + seed + System.lineSeparator());
        } catch (IOException e) {
            gui.appendLog("Encrypt failed! Cannot create file.");
        }
    }

}