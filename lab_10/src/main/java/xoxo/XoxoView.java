package xoxo;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextArea;

/**
 * This class handles most of the GUI construction.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoView {

    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;


    /**
     * A field to be the input of the seed.
     */
    private JTextField seedField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField;

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    //TODO: You may add more components here

    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {
            frame = new JFrame("Xoxo Decryptor or Encryptor");
            frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            frame.setLayout(new BorderLayout());
            Panelatas = new JPanel(new FlowLayout(FlowLayout.CENTER));
            Panelbawah = new JPanel(new GridLayout(1, 1));
            JPanel Panelteks = new JPanel(new GridLayout(3,1));
            JPanel messagePanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
            JPanel keyPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
            JPanel seedPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
            JPanel ePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
            JPanel dPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
            JPanel Tombol = new JPanel(new GridLayout(3, 1));
            JLabel messageLabel = new JLabel("Message    :");
            JLabel keyLabel = new JLabel("Key            :");
            JLabel seedLabel = new JLabel("Seed          :");
            messageField = new JTextField();
            messageField.setPreferredSize(new Dimension(200,25));
            keyField = new JTextField();
            keyField.setPreferredSize(new Dimension(200,25));
            seedField = new JTextField();
            seedField.setPreferredSize(new Dimension(200,25));
            encryptButton = new JButton("Encrypt");
            encryptButton.setPreferredSize(new Dimension(90, 30));
            decryptButton = new JButton("Decrypt");
            decryptButton.setPreferredSize(new Dimension(90, 30));
            logField = new JTextArea("=== Log ===\n");
            JScrollPane scroll = new JScrollPane(logField);
            scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
            scroll.setPreferredSize(new Dimension(400,200));

            messagePanel.add(messageLabel);
            messagePanel.add(messageField);
            keyPanel.add(keyLabel);
            keyPanel.add(keyField);
            seedPanel.add(seedLabel);
            seedPanel.add(seedField);

            Panelteks.add(messagePanel);
            Panelteks.add(keyPanel);
            Panelteks.add(seedPanel);

            ePanel.add(encryptButton);
            dPanel.add(decryptButton);

            Tombol.add(ePanel);
            Tombol.add(dPanel);

            Panelatas.add(Panelteks);
            Panelatas.add(Tombol);
            Panelbawah.add(scroll);
            frame.add(Panelatas, BorderLayout.NORTH);
            frame.add(Panelbawah, BorderLayout.CENTER);
            frame.pack();
            frame.setVisible(true);
        }

    }

    /**
     * Gets the message from the message field.
     *
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     *
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Gets the seed text from the key field.
     *
     * @return The input key string.
     */
    public String getSeedText() {
        return seedField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     *
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     *
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }

    public void Warning(String warning) {
        JOptionPane.showMessageDialog(frame, warning);
    }
}
