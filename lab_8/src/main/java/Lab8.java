import java.util.Scanner;
import Korporasi.Korporasi;

class Lab8 {
    public static void main(String[] args) {
                Scanner input = new Scanner(System.in);
                try {
                    int gajimax = Integer.parseInt(input.nextLine());
                    if (gajimax > 0) {
                        Korporasi.setGajimax(gajimax);
                        System.out.println("Batas gaji maksimal STAFF: " + gajimax);
                    }while (input.hasNextLine()) {
                        String[] input_split = input.nextLine().split(" ");
                        if (input_split[0].toUpperCase().equals("TAMBAH_KARYAWAN")) {
                            Korporasi.add(input_split[2], input_split[1].toUpperCase(), Integer.parseInt(input_split[3]));
                        } else if (input_split[0].toUpperCase().equals("STATUS")) {
                            Korporasi.status(input_split[1]);
                        } else if (input_split[0].toUpperCase().equals("GAJIAN")) {
                            Korporasi.gajian();
                        } else if (input_split[0].toUpperCase().equals("TAMBAH_BAWAHAN")) {
                            Korporasi.tambahBawahan(input_split[1], input_split[2]);
                        } else {
                            System.out.println("FORMAT MASUKAN SALAH!");
                        }
                    }
                } catch (Exception e) {
                    System.out.println("FORMAT MASUKAN SALAH!");
                }
            }
        }
    }
}