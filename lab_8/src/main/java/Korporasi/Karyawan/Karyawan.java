import java.util.ArrayList;

import Korporasi.Karyawan.Manager.Manager;
import Korporasi.Karyawan.Staff.Staff;
import Korporasi.Karyawan.Intern.Intern;



public class Karyawan {
    private String nama;
    private int gaji;
    private int bawahan;
    private String type;
    private int itunggaji = 0;
    private ArrayList<Employee> parabawahan = new ArrayList<Employee>();

    public Karyawan(String nama, int gaji){
        this.nama=nama;
        this.gaji=gaji;
        if (this instanceof Staff || this instanceof Manager) {
            this.bawahan = 10;
        } else if (this instanceof Intern) {
            this.bawahan = 0;
        }
    }
    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getGaji() {
        return gaji;
    }

    public void setGaji(int gaji) {
        this.gaji = gaji;
    }

    public int getBawahan() {
        return bawahan;
    }

    public void setBawahan(int bawahan) {
        this.bawahan = bawahan;
    }
    public int getItunggaji() {
        return itunggaji;
    }

    public void setItunggaji(int itunggaji) {
        this.itunggaji = itunggaji;
    }

    public ArrayList<Employee> getParabawahan() {
        return parabawahan;
    }

    public void setParabawahan(ArrayList<Employee> parabawahan) {
        this.parabawahan = parabawahan;
    }
public void Gajian(int gaji){
        itunggaji++;
        if (itunggaji==6) {
            this.setItunggaji(0);
            naikgaji();
        }
    }
public void naikgaji(){
    int gajibaru = (gaji*0.1) + gaji;
    System.out.println(name + " mengalami kenaikan gaji sebesar 10% dari " + gaji + " menjadi " + gajibaru);
    gaji = gajibaru;
}
    public boolean recruit(Karyawan pegawai) {
        if (this instanceof Manager) {
            if (pegawai instanceof Staff || pegawai instanceof Intern) {
                return true;
            }
        } else if (this instanceof Staff) {
            if (pegawai instanceof Intern) {
                return true;
            }
        }
        return false;
    }
}