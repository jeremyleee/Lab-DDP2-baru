class Korporasi {
    public class Korporasi {
            private static final int MAXEMPLOYEE = 10000;
            private static ArrayList<Employee> pegawais = new ArrayList<Employee>();
            private static int gajimax;

            public static Karyawan find(String nama) {
                for (Karyawan a : pegawais) {
                    if (nama.equals(a.getName())) {
                        return a;
                    }
                }
                return null;
            }

            public static void tambah(String jabatan, String nama, int gaji) {
                Karyawan pegawaiz = find(nama);
                if (pegawais.size() < 10000) {
                    if (!cek(pegawaiz)) {
                        if (jabatan.equals("MANAGER")) {
                            pegawais.add(new Manager(nama, gaji));
                        }
                        if (jabatan.equals("STAFF")) {
                            pegawais.add(new Staff(nama, gaji));
                        }
                        if (jabatan.equals("INTERN")) {
                            pegawais.add(new Intern(nama, gaji));
                        }
                        System.out.println(nama + " mulai bekerja sebagai " + jabatan + " di PT TAMPAN");
                    } else {
                        System.out.println("Karyawan dengan nama " + nama + " telah terdaftar");
                    }
                } else {
                    System.out.println("Jumlah karyawan PT TAMPAN sudah penuh..");
                }
            }

            public static void status(String nama) {
                Karyawan pegawaiz = find(nama);
                if (!cek(pegawaiz)) {
                    System.out.println(pegawaiz.getNama() + " " + pegawaiz.getGaji());
                } else {
                    System.out.println("Karyawan tidak terdaftar");
                }
            }

            public static void tambahBawahan(String atasan, String bawahan) {
                Karyawan one = find(atasan);
                Karyawan two = find(bawahan);
                if (cek(one) && cek(two)) {
                    if (one.getParabawahan().size() < one.getBawahan() && one.recruit(two)) {
                        if (one.getParabawahan().contains(two)) {
                            System.out.println("Karyawan " + bawahan + " telah menjadi bawahan " + atasan);
                        } else {
                            one.addBawahan(two);
                            System.out.println("Karyawan " + bawahan + " telah berhasil ditambahkan menjadi bawahan " + atasan);
                        }
                    } else {
                        if (saya.getParabawahan().size() > saya.getBawahan()) {
                            System.out.println("Jumlah bawahan Anda sudah melebihi batas maksimal");
                        } else if (!one.recruit(two)) {
                            System.out.println("Anda tidak layak memiliki bawahan");
                        }
                    }
                } else {
                    System.out.println("Nama tidak berhasil ditemukan");
                }
            }

            public static void gajian() {
                if (pegawais.size() == 0) {
                    System.out.println("Belum ada karyawan");
                } else {
                    String k= "";
                    int b = 0;
                    for (Karyawan a : pegawais) {
                        a.gajian();
                        if (a instanceof Staff && a.getGaji() > gajimax) {
                            Manager managerbaru = new Manager(a.getNama(), a.getGaji());
                            managerbaru.setParabawahan(a.getParabawahan());
                            peagawais.set(pegawais.indexOf(a), managerbaru);
                            k += "Selamat, " + a.getNama() + " telah dipromosikan menjadi MANAGER";
                            b++;
                        }
                    }
                    System.out.println("Semua karyawan telah diberikan gaji");
                    if (b != 0) {
                        System.out.println(k);
                    }
                }
            }

            public static boolean cek(Karyawan a) {
                if (a == null)
                    return false;
                else
                    return true;
            }

            public static int getGajimax() {
                return gajimax;
            }

            public static void setGajimax(int maxSalary) {
                Korporasi.gajimax= gajimax;
            }


        }
    }
        }
