package character;
public class Monster extends Player {
    private String roar;

    public Monster(String name, int hp) {
        super(name, hp * 2);
        this.name = name;
        this.hp = hp;
        this.roar="AAAAAAaaaAAAAAaaaAAAAAA";
        setType("Monster")
    }

    public Monster(String name, int hp, String roar) {
        super(name, hp * 2);
        this.roar = roar;
        setType("Monster")
    }

    public String roar() {
        return roar;
    }

    public void eat(Player musuh) {
        if (musuh.getHp() == 0) {
            this.setHp(this.getHp() + 15);
            listd.add(musuh);
        }
    }
}