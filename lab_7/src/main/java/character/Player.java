package character;
import java.util.ArrayList;

public class Player{
    protected String name;
    protected int hp;
    protected ArrayList<Player> listd = new ArrayList<Player>();
    protected String type;
    protected boolean isMatang;
    protected String roar;
    public Player (String name,String type,int hp){
        this.name=name;
        this.type=type;
        this.hp=hp;
    }

    public void attack(Player musuh){
        if (musuh.getType().equals("Magician")) {
            musuh.setHp(this.getHp() - 20);
        }else {
            musuh.setHp(musuh.getHp() - 10);
        }
    }
    public void eat(Player musuh) {
        if (musuh.getHp() == 0 & musuh.isMatang() == true) {
            this.setHp(this.getHp() + 15);
            listd.add(musuh);
        }
    }
    public String burn(Player musuh){
        if (musuh.getHp() == 0) {
            musuh.setMatang(true);
            return "Nyawa " + musuh.getName() + " 0 \n dan matang";
        } else {
            if (musuh.getType().equals("Magician")) {
                musuh.setHp(musuh.getHp() - 20);
            } else {
                musuh.setHp(musuh.getHp() - 10);
            }
            if (musuh.getHp() == 0) {
                musuh.setMatang(true);
                return "Nyawa " + musuh.getName() + " 0 \n dan matang";
            }else{
                return "Nyawa " + musuh.getName() + " " + musuh.getHp();
            }
        }
    }
    public String roar(){

    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        if (hp <= 0) {
        this.hp = 0;
        }else{
        this.hp = hp;}
        }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    public boolean isMatang() {
        return isMatang;
    }
    public ArrayList<Player> getListd() {
        return listd;
    }

    public void setListd(ArrayList<Player> listd) {
        this.listd = listd;
    }

    public void setMatang(boolean matang) {
        isMatang = matang;
    }

}