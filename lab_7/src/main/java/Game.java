import character.*;
import java.util.ArrayList;

public class Game{
    ArrayList<Player> player = new ArrayList<Player>();
    
    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name) {
        for (Player a : player) {
            if (a.getName().equals(name)) {
                return a;
            }
        }
            return null;
    }
    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp) {
        if (find(chara) == null) {
            player.add(new Player(chara, tipe, hp));
            return chara + " ditambah ke game";
        } else {
            return "Sudah ada karakter bernama " + chara;
        }
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar) {
        String aa = "";
        if (tipe != "Monster") {
            aa += "Karakter bukan Monster!";
        } else {
            if (find(chara) == null) {
                player.add(new Player(chara, tipe, hp));
                aa += chara + " ditambah ke game";
            } else {
                aa += "Sudah ada karakter bernama " + chara;
            }
        }
        return aa;
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){
        Player one = find(chara);
        if(one != null){
            player.remove(one);
            return one.getName() + " dihapus dari game";
        }
        else {
            return "Tidak ada " + chara;
        }
    }


    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara){
        Player one = find(chara);
        String ac = "";
        if(one != null){
            ac += (one.getType() + " " + one.getName() + "\n" + "HP: " + one.getHp() + "\n");
            if(one.getHp() == 0){
                ac += "Sudah meninggal dunia dengan damai\n";
            }
            else{
                ac += "Masih hidup\n";
            }
            ac += diet(chara);
        }
        return ac ;
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
        String ad = "";
        if(player.size() > 0){
            ad += "\n";
            for(Player one : player){
                ad += status(one.getName());
            }
        }
        else{
            ad += "Tidak ada pemain";
        }
        return ad; //kenapa harus pake another variable ya(?)
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public StringBuffer diet(String chara){
        Player one = find(chara);
        StringBuffer ab = new StringBuffer();
        if(one!= null) {
            if (one.getListd().size() != 0) {
                ab.append("Memakan");
                for (Player termakan : one.getListd()) {
                    ab.append(termakan.getType()).append(" ").append(termakan.getName()).append(", ");
                }
                ab.delete(ab.length() - 2, ab.length());
                ab.append("\n");
            } else {
                ab.append("Belum memakan siapa-siapa\n");
            }
        }
        else {
        ab.append("Tidak ada ").append(chara);
    }
        return ab;}
    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
        String k = "";
        if (player.size() == 0) {
            k = "Belum memakan siapa-siapa";
        } else {
            for (Player a : player) {
                k += "Memakan " + " " + a.getType() + " " + a.getName() + "\n";
            }
        }
        return k;
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName) {
        Player one=find(meName);
        Player two=find(enemyName);
        one.attack(two);
        return "Nyawa "+two.getName()+" 15" ;
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName) {
        Player one = find(meName);
        Player two = find(enemyName);
        if (one.getType()!="Magician"){
            return "Bukan Magician,tidak bisa membakar";
        }
        else {
            if (one == null) {
                return "Tidak ada " + meName;
            } else if (two == null) {
                return "Tidak ada " + enemyName;
            } else {
                return one.burn(two);
            }
        }
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
        Player one=find(meName);
        Player two=find(enemyName);
        if (one == null) {
            return "Tidak ada " + meName;
        } else if (two == null) {
            return "Tidak ada " + enemyName;
        } else {
            if(two.getHp()!=0){
                return one.getName()+" tidak bisa memakan "+two.getName();
            }
            one.eat(two);
            return "Mons memakan "+two.getName()+"\nNyawa "+one.getName()+" kini "+one.getHp();
        }
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
        Player one=find(meName);
        if(one.getType()!="Monster"){
            return meName+" tidak bisa berteriak";
        }
        if (one == null) {
            return "Tidak ada " + meName;
        } else {
            return one.roar();
        }
    }
}

