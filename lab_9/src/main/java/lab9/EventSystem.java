package lab9;

import lab9.user.User;
import lab9.event.Event;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.math.BigInteger;
import java.text.ParseException;
/**
* Class representing event managing system
*/
public class EventSystem
{
    /**
    * List of events
    */
    private ArrayList<Event> events;
    
    /**
    * List of users
    */
    private ArrayList<User> users;
    /**
    * Constructor. Initializes events and users with empty lists.
    */
    public EventSystem()
    {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }
    public User getUser(String name) {
        for (User a : users) {
            if (a.getName().equals(name)) {
                return a;
            }
        }
        return null;
    }

    public Event getEvent(String name) {
        for (Event a : events) {
            if (a.getName().equals(name)) {
                return a;
            }
        }
        return null;
    }
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr) {
        try {
            SimpleDateFormat ab = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
            Date dateStart = ab.parse(startTimeStr);
            Date dateEnd = ab.parse(endTimeStr);

        if (getEvent(name) != null) {
            return ("Event " + name + " sudah ada!");
        }
        else {
            if (dateStart.compareTo(dateEnd) > 0) {
                return ("Waktu yang diinputkan tidak valid!");
            } else {
                events.add(new Event(name, dateStart,dateEnd,costPerHourStr));
                return ("Event " + name + " berhasil ditambahkan!");
            }
        }
    }
    catch(ParseException e){
            return ("Can't parse input");
        }
        }
        public String addUser (String name) {
                if (getUser(name)!=null) {
                    return ("User " + name + " sudah ada!");
                } else {
                    users.add(new User(name));
                    return ("User " + name + " berhasil ditambahkan!");
                }
            }

        public String registerToEvent (String userName, String eventName)
        {
                    if (getUser(userName)==null & getEvent(eventName)==null) {
                        return ("Tidak ada pengguna dengan nama " + userName + " dan acara dengan nama " + eventName + "!");
                    } else {
                        if (getUser(userName)!=null) {
                            if (getEvent(eventName)!=null) {
                                if(getUser(userName).addEvent(getEvent(eventName))==false){
                                    return (userName+" sibuk sehingga tidak dapat menghadiri "+eventName+"!");
                                }
                                else{
                                    return(userName+" berencana menghadiri "+eventName+"!");
                                }

                            } else {
                                return ("Tidak ada acara dengan nama " + eventName + "!");
                            }
                        } else {
                            return ("Tidak ada pengguna dengan nama " + userName + "!");
                        }
                    }
                }
}