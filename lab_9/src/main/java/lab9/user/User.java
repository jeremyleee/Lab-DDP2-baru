package lab9.user;
import java.math.BigInteger;
import lab9.event.Event;
import java.util.ArrayList;
import java.util.Collections;

/**
* Class representing a user, willing to attend event(s)
*/
public class User
{
    /** Name of user */
    private String name;
    
    /** List of events this user plans to attend */
    private ArrayList<Event> events;
    
    /**
    * Constructor
    * Initializes a user object with given name and empty event list
    */
    public User(String name)
    {
        this.name = name;
        this.events = new ArrayList<>();
    }
    
    /**
    * Accessor for name field
    * @return name of this instance
    */
    public String getName() {
        return name;
    }
    
    /**
    * Adds a new event to this user's planned events, if not overlapping
    * with currently planned events.
    *
    * @return true if the event if successfully added, false otherwise
    */
    public boolean addEvent(Event newEvent)
    {
        for(Event a:events) {
            if (a.overlaps(newEvent) == true) {
                return false;
            }
        }
        events.add(newEvent);
                    return true;
                }

    /**
    * Returns the list of events this user plans to attend,
    * Sorted by their starting time.
    * Note: The list returned from this method is a copy of the actual
    *       events field, to avoid mutation from external sources
    *
    * @return list of events this user plans to attend
    */
    public ArrayList<Event> getEvents()
    {
        ArrayList<Event> newlist = (ArrayList<Event>) this.events.clone();
        Collections.sort(newlist, (o1, o2) -> o1.getStart().compareTo(o2.getEnd()));
        return newlist;
    }
    public BigInteger getTotalCost(){
        BigInteger total=new BigInteger("0");
        for(Event a:events){
            BigInteger berubah=new BigInteger(a.getBiaya());
           total= total.add(berubah);
        }
        return total;
    }
}
