package lab9.event;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.math.BigInteger;
import java.text.DateFormat;
/**
* A class representing an event and its properties
*/
public class Event {
    /**
     * Name of event
     */
    private String name;
    private String startTime;
    private String endTime;
    private String biaya;
    private Date start;
    private Date end;
    // TODO: Make instance variable for cost per hour

    /**
     * Accessor for name field.
     *
     * @return name of this event instance
     */
    public Event(String name, Date start, Date end, String biaya) {
        this.name = name;
        this.end = end;
        this.start = start;
        this.biaya = biaya;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartTime() {
        return toNewDate(this.start);
    }

    public String getEndTime() {
        return toNewDate(this.end);
    }

    public String getBiaya() {
        return biaya;
    }

    public void setBiaya(String biaya) {
        this.biaya = biaya;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public String toString() {
        return (this.name + "\nWaktu mulai: " + toNewDate(this.start)  + "\nWaktu selesai: " + toNewDate(this.end) + "\nBiaya kehadiran: " + this.biaya);
    }

    public String toNewDate(Date a) {
        String newstart = new SimpleDateFormat("dd-MM-yyy, HH:mm:ss").format(a);
        return newstart;
    }

    public boolean overlaps(Event event2) {
        if (this.start.compareTo(event2.end) >= 0 || this.end.compareTo(event2.start) <= 0) {
            return false;
        } else {
            return true;
        }

    }
}
        // HINT: Implement a method to test if this event overlaps with another event
        //       (e.g. boolean overlapsWith(Event other)). This may (or may not) help
        //       with other parts of the implementation.